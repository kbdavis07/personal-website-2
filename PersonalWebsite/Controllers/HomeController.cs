﻿using PersonalWebsite.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PersonalWebsite.Controllers
{
    public class HomeController : Controller
    {
        
        
        public ActionResult Index()
        {
           ViewBag.Title = "Home Page";
           return View();
        }

        public ActionResult Fav()
        {
            ViewBag.Title = "Favorite Websites";
            return View();
        }


        public ActionResult Books()
        {
            ViewBag.Title = "Favorite Books";
            return View();
        }


        public ActionResult Photos()
        {
            ViewBag.Title = "Photo Album";
            return View();
        }

        [HttpGet]
        public ActionResult Login()
        {
            ViewBag.Title = "Login";
            return View();
        }


        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
       public ActionResult Login(LoginModel model)
        {
            ViewBag.Title = "Login Submitted";

            //Todo: Is Input validation needed?

            var UserName = model.UserName;
            var Password = model.Password;

            UserName = UserName.ToLowerInvariant();
            Password = Password.ToLowerInvariant();

            if (UserName == "joe" && Password == "doe")  // This is case sensitive!
                {
                   // Login is correct now saving login info!

                    //Storing in User Session first just in case Cookies are not enabled.
                    Session["UserName"] = model.UserName;
                    Session["Password"] = model.Password;


                   //Storing in User Cookie
                    HttpCookie UserCookie = new HttpCookie("UserName");
                    HttpCookie PassCookie = new HttpCookie("Password");
                    
                    UserCookie.Value = Session["UserName"].ToString();
                    PassCookie.Value = Session["Password"].ToString();
                        
                    UserCookie.Expires = DateTime.Now.AddDays(366.0);
                    PassCookie.Expires = DateTime.Now.AddDays(366.0);
                    
                    this.ControllerContext.HttpContext.Response.Cookies.Add(UserCookie);
                    this.ControllerContext.HttpContext.Response.Cookies.Add(PassCookie);

                    return RedirectToAction("Confirmation", "Home");
                
                }
            else
                  // Wrong Login Info Entered
                  ViewBag.Title = "Invalid Login";
                  Session["UserName"] = "Invalid";
                  return RedirectToAction("Login", "Home");
        }


        public ActionResult Confirmation()
        {
            ViewBag.Title = "Login Confirmation";
            return View();
        }


        [HttpGet]
        public ActionResult Settings()
        {
            ViewBag.Title = "Change Site Theme";
            ViewBag.Message = "Change Site Theme";
            return View();
        }

        [HttpPost]
        public ActionResult Settings(SiteModels model)
        {
            
            //Add User Selection to Variable Theme
            var Theme = model.ThemeSelect.ToString();
            
            //First Add to Session
            Session["Theme"] = (Theme + ".css");

            //Second Add to User Cookie
            HttpCookie cookie = new HttpCookie("Theme");
            cookie.Value = Session["Theme"].ToString();
            cookie.Expires = DateTime.Now.AddDays(366.0);
            this.ControllerContext.HttpContext.Response.Cookies.Add(cookie);

            ViewBag.Title = ("Current Theme : " + Theme);
            ViewBag.Message = "Change Site Theme";

            return View();
        }



    }
}