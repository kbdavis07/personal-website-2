﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PersonalWebsite.Models;


namespace PersonalWebsite.Controllers
{
    public class GuestController : Controller
    {
        private GuestBook db = new GuestBook();

        
        // GET: Guest/Create
        public ActionResult Index()
        {
            //Sorts Comments by most recent and only return the first 20.
            var mostRecentEntries = (from entry in db.Entries orderby entry.DateAdded descending select entry).Take(20);

            var NumberOfComments = db.Entries.Count();

            ViewData["Count"] = NumberOfComments;
            ViewData["Comments"] = mostRecentEntries.ToList();
            return View();
        }

        // POST: Guest/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index([Bind(Include = "Id,Name,Message")] GuessBook guessBook)
        {
            if (ModelState.IsValid)
            {
                guessBook.DateAdded = DateTime.UtcNow;
                db.Entries.Add(guessBook);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(guessBook);
        }

        

        // GET: Guest/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            GuessBook guessBook = db.Entries.Find(id);
            if (guessBook == null)
            {
                return HttpNotFound();
            }
            return View(guessBook);
        }

        // POST: Guest/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            GuessBook guessBook = db.Entries.Find(id);
            db.Entries.Remove(guessBook);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
