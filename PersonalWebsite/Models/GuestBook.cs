namespace PersonalWebsite.Models
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.Data.Entity;
    using System.Linq;

    public class GuestBook : DbContext
    {
        // Your context has been configured to use a 'GuestBook' connection string from your application's 
        // configuration file (App.config or Web.config). By default, this connection string targets the 
        // 'PersonalWebsite.Models.GuestBook' database on your LocalDb instance. 
        // 
        // If you wish to target a different database and/or database provider, modify the 'GuestBook' 
        // connection string in the application configuration file.
        public GuestBook()
            : base("name=GuestBook")
        {
        }

        // Add a DbSet for each entity type that you want to include in your model. For more information 
        // on configuring and using a Code First model, see http://go.microsoft.com/fwlink/?LinkId=390109.

        public virtual DbSet<GuessBook> Entries { get; set; }
    }

    public class GuessBook
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public string Message { get; set; }

        public DateTime DateAdded { get; set; }
 
    }
}