﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace PersonalWebsite.Models
{
    //Fills the Drop Down Menu for Theme
    public enum ThemeSelect
    {

        [Display(Name = "Default")]
        Default,

        [Display(Name = "Amelia")]
        amelia,

        [Display(Name = "Cerulean")]
        cerulean,

        [Display(Name = "Cosmo")]
        cosmo,

        [Display(Name = "Cyborg")]
        cyborg,

        [Display(Name = "Darkly")]
        darkly,

        [Display(Name = "Flatly")]
        flatly,

        [Display(Name = "Journal")]
        journal,

        [Display(Name = "Lumen")]
        lumen,

        [Display(Name = "Readable")]
        readable,

        [Display(Name = "Simplex")]
        simplex,

        [Display(Name = "Slate")]
        slate,

        [Display(Name = "Space Lab")]
        spacelab,

        [Display(Name = "Super Hero")]
        superhero,

        [Display(Name = "United")]
        united,

        [Display(Name = "Yeti")]
        yeti

    }
    
    public class SiteModels
    {
        //Drop Down Menu for Changing Theme
        [Display(Name = "Change Theme")]
        public ThemeSelect? ThemeSelect { get; set; }
    }
}